<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    public function selamat(Request $request)
    {
        $namaDepan = $request->input('firstname');
        $namaBelakang = $request->input('lastname');

        return view('main', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
